//p5 maths functions actually used. 
//external for modularity of main classes.
export default class MathFunctions{
    //give it the p5 environment to work with.
    constructor(_p5){ this.p5 = _p5;}

    //random generation
    randomSeed (seed){ return this.p5.randomSeed(seed); }

    //used as either:
    //random (maxValue)
    //random (minValue,maxValue)
    random (a,b){ return this.p5.random(a,b); }

    //unitary vector.
    random2D (){ return this.p5.__proto__.constructor.Vector.random2D(); }

    floor (x){ return this.p5.floor(x); }

    //used as either:
    //createVector() as a shortcut for createVector(0,0) 
    //createVector(x,y)
    createVector(x,y){ return this.p5.createVector(x,y); }

    colorHSL (h,s,l){ return this.p5.color("hsl(" + h + "," + s + "%," + l + "%)"); } 
    color (s){ return this.p5.color(s) ;}
}
