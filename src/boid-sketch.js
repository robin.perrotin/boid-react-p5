import React from 'react';
import Sketch from 'react-p5';

import Switch from '@material-ui/core/Switch';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import Boid from './boid';
import MathFunctions from './MathFunctions';

//import from 'p5-react-renderer';

import './styles.css';


export default class BoidSketch extends React.Component {

    static defaultProps = {
	width : 400,
	height : 400,
	options : true,
    };
    
    croppedWidth = () => {return ((this.props.width > window.innerWidth) ? window.innerWidth : this.props.width); };
    croppedHeight = () => {return ((this.props.height > window.innerHeight) ? window.innerHeight : this.props.height); };
    
    constructor(props){
	super(props);
	
	this.state = {
	    /*
	    /* those are almost only relative to each other */
	    alignmentStr : this.props.alignmentStr || 1.0,
	    cohesionStr : this.props.cohesionStr || 1.0,
	    separationStr : this.props.separationStr || 1.0,

	    /* In pixel distance */
	    alignmentRadius : this.props.alignmentRadius || 60.0,
	    cohesionRadius : this.props.cohesionRadius || 90.0,
	    separationRadius : this.props.eparationRadius || 30.0,

	    smoothness : this.props.smoothness || 0.95,
	    maxSpeed : this.props.maxSpeed || 3.0,

	    drawingCircles : this.props.drawingCircles || false,
	    drawingVelArrows : this.props.drawingVelArrows || false,
	    drawingForceArrows : this.props.drawingForceArrows || false,
	    drawingTheOneArrows : this.props.drawingTheOneArrows || false,

	    n : this.props.n || 40
	};

	this.flock = []; 
    }

    setup = (p5,parentRef) => {
	p5.createCanvas(this.croppedWidth(),this.croppedHeight()).parent(parentRef);

	this.MF = new MathFunctions(p5);
	
	for (let i = 0; i < this.state.n; i++) {
	    this.flock.push(new Boid(this.MF,{width:p5.width,height:p5.height}));
	}

	if(this.props.options)
	    this.flock[0].makeItTheOne();
    };

    draw = p5 => {
	/**** for now, boids number are changed asap. can cause freeze. ****/

	while(this.flock.length > this.state.n){ //deleting boids :(
	    this.flock.pop();
	}
	
	while(this.flock.length < this.state.n){ //adding new boid.
	    this.flock.push(new Boid(this.MF,{width:p5.width,height:p5.height}));
	}
	    
	p5.background(0);
	for(let i = this.flock.length - 1; i >= 0; --i){
	    var boid = this.flock[i];
		
	    boid.edges();
	    boid.fflock(this.flock,this.state);
	    boid.update(this.state);
	    boid.show(p5,this.state);
	}
	
	if(this.state.n < 0) return;

    };

    handleVelArrows = (event,newValue) => {this.setState({drawingVelArrows:newValue});}
    handleCircles = (event,newValue) => {this.setState({drawingCircles:newValue});}
    handleForceArrows = (event,newValue) => {this.setState({drawingForceArrows:newValue});}
    handleTheOneArrows = (event,newValue) => {this.setState({drawingTheOneArrows:newValue});}

    handleChangeAlignmentStr = (event,newValue) => {this.setState({alignmentStr:newValue});}
    handleChangeCohesionStr = (event,newValue) => {this.setState({cohesionStr:newValue});}
    handleChangeSeparationStr = (event,newValue) => {this.setState({separationStr:newValue});}
    
    handleChangeAlignmentRadius = (event,newValue) => {this.setState({alignmentRadius:newValue});}
    handleChangeCohesionRadius = (event,newValue) => {this.setState({cohesionRadius:newValue});}
    handleChangeSeparationRadius = (event,newValue) => {this.setState({separationRadius:newValue});}

    handleChangeSmoothness = (event,newValue) => {this.setState({smoothness:newValue});}
    handleChangeMaxSpeed = (event,newValue) => {this.setState({maxSpeed:newValue});}
    
    handleChangeN = (event,newValue) =>{
	if(newValue < 0) return;
	this.setState({n:newValue});
    }
    
    render() {
	const sketch = <Sketch className={this.props.className || "BoidSketch"} style={{width:this.croppedWidth(),height:this.croppedHeight()}} setup={this.setup} draw={this.draw} />;
	
	return (
	    this.props.options ? (
		<Box className="Boid App">    
		  <Box bgcolor="darkgray"> {this.props.title? <h1>{this.props.title}</h1> : {}} </Box>
		  <Box component="span" display="flex" flexWrap="wrap" justifyContent="center">
		    <Box bgcolor="darkgray" style={{padding:"10px","borderRadius":"20px"}}> {sketch} </Box>
		    <Box flexWrap="nowrap" bgcolor="darkgray" justifyContent="center" style={{padding:"10px","borderRadius":"20px",alignSelf:"center", margin:"20px"}} >
		    <Grid container item spacing={3} direction="row" justify="center">  
		      <Grid item xl={2} style={{minWidth:"260px"}} >
			<Box><p>Alignment Force Strength: {this.state.alignmentStr}</p><Slider min={0.0} max={5.0} step={0.05} style={{width:"150px"}} value={this.state.alignmentStr} onChange={this.handleChangeAlignmentStr} /> </Box>
			<Box><p>Cohesion Force Strength: {this.state.cohesionStr}</p><Slider min={0.0} max={5.0} step={0.05} style={{width:"150px"}} value={this.state.cohesionStr} onChange={this.handleChangeCohesionStr} /></Box>
			<Box><p>Separation Force Strength: {this.state.separationStr}</p><Slider min={0.0} max={5.0} step={0.05} style={{width:"150px"}} value={this.state.separationStr} onChange={this.handleChangeSeparationStr} /></Box>
			<Box><Switch color="primary" value={this.state.drawingVelArrows} onChange={this.handleVelArrows}/> Velocity Arrows</Box>
			<Box><Switch color="primary" value={this.state.drawingForceArrows} onChange={this.handleForceArrows}/> Forces Arrows</Box>
		      </Grid>
		      <Grid item xl={2} style={{minWidth:"260px"}} >
			<Box><p>Alignment Radius: {this.state.alignmentRadius}</p><Slider min={0} max={100} style={{width:"150px"}} value={this.state.alignmentRadius} onChange={this.handleChangeAlignmentRadius} /></Box>
			<Box><p>Cohesion Radius: {this.state.cohesionRadius}</p><Slider min={0} max={100} style={{width:"150px"}} value={this.state.cohesionRadius} onChange={this.handleChangeCohesionRadius} /></Box>
			<Box><p>Separation Radius: {this.state.separationRadius}</p><Slider min={0} max={100} style={{width:"150px"}} value={this.state.separationRadius} onChange={this.handleChangeSeparationRadius} /></Box>
			<Box><Switch color="primary" value={this.state.drawingCircles} onChange={this.handleCircles}/> Red's Radius Circles</Box> 
			<Box><Switch color="primary" value={this.state.drawingTheOneArrows} onChange={this.handleTheOneArrows}/> Red's Arrows</Box>
		      </Grid>
		      <Grid item xl={2} style={{minWidth:"260px"}} >
			<Box><p>Smoothness: {this.state.smoothness}</p><Slider min={0.83} max={0.998} step={0.001} style={{width:"150px"}} value={this.state.smoothness} onChange={this.handleChangeSmoothness}/></Box>
			<Box><p>Maximum Speed : {this.state.maxSpeed}</p><Slider min={0.0} max={10.0} step={0.025} style={{width:"150px"}} value={this.state.maxSpeed} onChange={this.handleChangeMaxSpeed}/></Box>
			<Box><p>Number of Boids: {this.state.n}</p><Slider min={1} max={300} step={1} style={{width:"150px"}} value={this.state.n} onChange={this.handleChangeN} /></Box>
		      </Grid>
		    </Grid>
		    </Box>
		  </Box>
		</Box>
	    ) : sketch );
    };
}
