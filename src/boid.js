

export default class Boid {
    //for computation purposes, a boids lives in the rectangle (0,0) => (W,H) defined by its flock.
    //the geometric property of those boids is handled as a tore. it is not that nice for the distance function. but it looks nicer

    constructor(MF,flock,customInitialize){
	//mostly binding.
	this.MF = MF;
	this.flock = flock;

	this.pos = MF.createVector();
	this.vel = MF.createVector();
	this.acc = MF.createVector();

	//fwdDir; is either a normal vector of last trusted dir; or null.
	this.fwdDir = null;

	if(!customInitialize)
	    this.initializeRandomly();
    }

    recomputeFwdDir(){
	if(this.vel.magSq() !== 0) this.fwdDir = this.vel.copy().normalize(); 
    }
    
    //#TOREFACTOR put the random description in flock.js
    initializeRandomly(){
	var MF = this.MF;
	var flock = this.flock;
	this.initialize(
	    MF.createVector(MF.random(flock.width),MF.random(flock.height)),
	    MF.random2D().setMag(MF.random(1.0,3.0)),
	    MF.colorHSL(MF.floor(MF.random(60,180)),MF.floor(MF.random(50,90)),50),
	    MF.random(6.5,10.0)
	);

	return this;
    }

    //could split maths and display variables in two different initializers? maybe split all drawing
    initialize(posI,velI,color,size){
	this.pos.x = posI.x;
	this.pos.y = posI.y;
	this.edges();
	
	this.vel.x = velI.x;
	this.vel.y = velI.y;
	this.recomputeFwdDir();

	this.color = color;
	this.size = size;

	return this;
    }

    //to refactor. should be a decision on the draw function not an inherent property.
    makeItTheOne(){
	this.isTheOne = true;
	this.color = "red";
	this.size = 8.0;
    }

    //#TOREFACTOR put clipping functions into MF.
    edges() {
	var flock = this.flock;

	if (this.pos.x > flock.width) {
	    this.pos.x %= flock.width;
	} else if (this.pos.x < 0) {
	    this.pos.x = (this.pos.x) % flock.width + flock.width;
	} 
	if (this.pos.y > flock.height) {
	    this.pos.y %= flock.height;
	} else if (this.pos.y < 0) {
	    this.pos.y = (this.pos.y) % flock.height + flock.height;
	}
    }

    distanceToSq(pos2,isToric){
	return this.closestRepresentative(pos2,isToric).sub(this.pos).magSq();
    }

    //#TOREFACTOR put closest1D in MF.
    closestRepresentative(pos2,isToric){
	if(isToric){
	    var flock = this.flock;
	    return this.MF.createVector(
		(pos2.x > this.pos.x ?
		                    (pos2.x - this.pos.x > flock.width/2.0 ? pos2.x - flock.width : pos2.x )
		                  : (this.pos.x - pos2.x > flock.width/2.0 ? pos2.x + 1.0 * flock.width : pos2.x )),
		(pos2.y > this.pos.y ?
		                    (pos2.y - this.pos.y > flock.height/2.0 ? pos2.y - flock.height : pos2.y )
		                  : (this.pos.y - pos2.y > flock.height/2.0 ? pos2.y + 1.0 * flock.height : pos2.y ))
	    );
	}else{ return pos2; };
    }
/*
    utilSteeringForEachIfInRange(boids,distF,d,callback){
	for(let other of boids){
	    if( distF(this,other) <= d ){
		callback(this,other,d);
	    }
	}
    }

    utilSteeringBarycentric(steering,total,maxSteering){
	steering.div(total);
	steering.sub(this.vel);
	steering.limit(maxSteering);
    }

    //#TOREFACTOR add dynamic matrix to store distance for multiple calculs?
    //compute the align steering vector exerced by all objects in boids.
    //expose meta parameters.
    alignRefactored(boids,radiusSq,roughness,maxSteering){
	let steering = this.MF.createVector();
	let total = 0;

	this.utilSteeringForEachIfInRange(boids,
					  (boidThis,boidOther) => {return boidThis.distanceToSq(boidOther.pos,true);},
					  radiusSq,
					  (boidThis,boidOther,d) => { steering.add(boidOther.vel); total++; }
					 );

	if(total > 0){
	    if(steering.magSq() == 0) //perfect storm, the steering force is perfectly 0.
		return steering;
	    
	    return this.utilSteeringBarycentric(steering,total,maxSteering);
	}
	return null; //return null explicitely when that force doesn't exist.   
    }*/
    
    align(boids,perceptionRadiusSq,roughness,maxSpeed){
	let steering = this.MF.createVector();
	let total = 0;

	for(let other of boids){
	    if(other === this) continue;
	    let d = this.distanceToSq(other.pos,true);
	    if(d < perceptionRadiusSq){
		steering.add(other.vel);
		total++;
	    }
	}
	if(total > 0 && steering.magSq() > 0){
	    steering.div(total);
//	    steering.setMag(maxSpeed);
	    steering.sub(this.vel);
	}
	return steering;
    }

    cohesion(boids,perceptionRadiusSq,roughness,maxSpeed){
	let steering = this.MF.createVector();
	let total = 0;

	for(let other of boids){
	    if(other === this) continue;
	    let d = this.distanceToSq(other.pos,true);
	    if(d < perceptionRadiusSq){
		steering.add(this.closestRepresentative(other.pos,true));
		total++;
	    }
	}
	
	if(total > 0 && steering.magSq() > 0){
	    steering.div(total);
	    steering.sub(this.pos);
//	    steering.normalize(maxSpeed);
//	    steering.setMag(maxSpeed);
	}
	return steering;
    }
    
    separation(boids,perceptionRadiusSq,roughness,maxSpeed){
	let steering = this.MF.createVector();
	let total = 0;

	for(let other of boids){
	    if(other === this) continue;
	    let d = this.distanceToSq(other.pos,true);
	    if(d < perceptionRadiusSq){
		let diff = this.MF.createVector().add(this.pos).sub(this.closestRepresentative(other.pos,true)).div(d);
		steering.add(diff);
		total += 1.0 / d;
	    }
	}
	
	if(total > 0 && steering.magSq() > 0){
	    steering.mult(total);
//	    steering.sub(this.pos);
//	    steering.setMag(maxSpeed);
//	    steering.sub(this.vel);
//	    steering.limit(roughness);
//	    steering.setMag(maxSpeed);
//	    steering.sub(this.vel);

	}
	return steering;
    }
    
    fflock(boids,ctx) {
	let alignment = ctx.alignmentRadius !== 0 && ctx.alignmentStr !== 0 ? this.align(boids,ctx.alignmentRadius * ctx.alignmentRadius || 50*50) : this.MF.createVector();
	let cohesion = ctx.cohesionRadius !== 0 && ctx.cohesionStr !== 0  ? this.cohesion(boids, ctx.cohesionRadius * ctx.cohesionRadius || 50*50) : this.MF.createVector();
	let separation = ctx.separationRadius !== 0 && ctx.separationStr !== 0 ? this.separation(boids, ctx.separationRadius * ctx.separationRadius || 100 * 100) : this.MF.createVector();

	alignment.mult((ctx.alignmentStr || 1.0) * 1.0);
	cohesion.mult((ctx.cohesionStr * 1.0 || 1.0) * 0.1);
	separation.mult((ctx.separationStr || 1.0) * 10000.0);

	this.velC = cohesion;
	this.velA = alignment;
	this.velS = separation;

	this.acc.add(alignment);
	this.acc.add(cohesion);
	this.acc.add(separation);
	
	this.accArrow = this.acc.copy();

	this.acc.limit(1.00 - ctx.smoothness);

    }
    
    update(ctx){
	this.pos.add(this.vel);
	this.vel.add(this.acc);
	this.vel.limit(ctx.maxSpeed || 1.0);
	this.recomputeFwdDir();
	this.acc.mult(0);
    }
    
    show(p5,ctx){
//	console.log(this.color.levels);
	p5.fill(this.color);
	//p5.nostroke()
	p5.noStroke();
	p5.beginShape();

	if(this.fwdDir){
	    //if this.dir is defined, arrow shape looking forward.
	    /* polar coordonates from x,y */ 
	    let pointList = [
		[-2*p5.PI/3.0,   this.size/2.0],
		[           0,   this.size    ],
		[ 2*p5.PI/3.0,   this.size/2.0]];

	    p5.beginShape();
	    for (let p of pointList){
		let coords = this.fwdDir.copy().setMag(p[1]).rotate(p[0]).add(this.pos); 
		p5.vertex(
		    coords.x,
		    coords.y
		);
	    }
	    p5.endShape(p5.CLOSE);
	}else{
	    //else, circle shape.
	    p5.circle(this.pos.x,this.pos.y,this.size * 0.75 ); //the 0.75 is not that important.
	}
	
	if(ctx.drawingVelArrows || (this.isTheOne && ctx.drawingTheOneArrows)){
	    this.drawArrow(p5,this.pos, this.vel.copy().mult(30), 'red');
	}
	if(ctx.drawingForceArrows || (this.isTheOne && ctx.drawingTheOneArrows)){
	    this.drawArrow(p5,this.pos, this.velA.mult(1), 'blue');
	    this.drawArrow(p5,this.pos, this.velC.mult(1), 'purple');
	    this.drawArrow(p5,this.pos, this.velS.mult(1), 'orange');
	    this.drawArrow(p5,this.pos, this.accArrow.mult(1), 'white');
	}

	
	if(this.isTheOne && ctx.drawingCircles){
	    let tabCoords = [[-1.0,1.0],[-1.0,0.0],[-1.0,-1.0],[0.0,-1.0],[1.0,-1.0],[1.0,0.0],[1.0,1.0],[0.0,1.0],[0.0,0.0]];
	    p5.noFill();
	    for(let i = 0; i < 9; ++i){
		let x = this.pos.x + tabCoords[i][0] * p5.width;
		let y = this.pos.y + tabCoords[i][1] * p5.height;
		if(ctx.alignmentRadius !== 0){
		    p5.stroke("blue");
		    p5.circle(x,y, 2*(ctx.alignmentRadius || 90));
		}
		if(ctx.cohesionRadius !== 0){
		    p5.stroke("purple");
		    p5.circle(x,y, 2*(ctx.cohesionRadius || 140));
		}
		if(ctx.separationRadius !== 0){
		    p5.stroke("orange");
		    p5.circle(x,y, 2*(ctx.separationRadius || 50));
		}
	    }
	}
    }

    drawArrow(p5,base, vec, myColor) {
	p5.push();
	p5.stroke(myColor);
	p5.strokeWeight(2);
	p5.fill(myColor);
	p5.translate(base.x, base.y);
	p5.line(0, 0, vec.x, vec.y);
	p5.rotate(vec.heading());
	let arrowSize = 4;
	p5.translate(vec.mag() - arrowSize, 0);
	p5.triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
	p5.pop();
    }
}

