import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import BoidSketch from './boid-sketch';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <BoidSketch
      width={600}
      height={600}
      
      n={120}

      className = "CustomBoidSketch"
      //style //custom style
      
      options={true} //this change the behavior totally.
                      //could be refactored into two components.
      title={"unoptimized intensive simulation"}

      />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
