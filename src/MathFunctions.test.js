import MathFunctions from './MathFunctions';
import p5 from "p5";
import 'jest-canvas-mock';
var _p5 = new p5();

//if you ever want to remove p5, here are the expected behaviors.


describe('p5 Math functions testings', () => {
    var MF;
    
    beforeAll(() => {
	return (MF = new MathFunctions(_p5));
    });

    test('random', () => {
	MF.randomSeed("seeding test"); //to be deterministic. shouldn't be needed though
	for(var i = 0; i < 100; ++i){
	    expect(MF.random(5)).toBeGreaterThanOrEqual(0);
	    expect(MF.random(12)).toBeLessThan(12);
	    expect(MF.random(3,6)).toBeGreaterThanOrEqual(3);
	    expect(MF.random(-4,-2)).toBeLessThan(-2);
	}
    });
    
    test('random2D', () => {
	MF.randomSeed("seeding test"); //to be deterministic. shouldn't be needed though
	for(var i = 0; i < 100; ++i){
	    expect(MF.random2D().mag()).toBeCloseTo(1,7);
	}
    });

    test('floor', () => {
	expect(MF.floor(3.862)).toEqual(3);
	expect(MF.floor(-2.2189)).toEqual(-3);
    });

    test('createVector', () => {
	let zero = MF.createVector();
	expect(zero.x).toEqual(0);
	expect(zero.y).toEqual(0);
	expect(zero.z).toEqual(0);

	let v = MF.createVector(3,4);
	expect(v.x).toEqual(3);
	expect(v.y).toEqual(4);
	expect(v.mag()).toEqual(5);
    });

    //maybe not necessary. more like compatible with drawing functions.
    test('color',() => {
	expect(MF.color("red").levels).toStrictEqual([255,0,0,255]);
	expect(MF.colorHSL(110,20,40).levels).toStrictEqual([88,122,82,255]);
    });
});

describe('p5.Vector Math functions testings', () => {
    var MF;
    
    beforeAll(() => {
	
	return (MF = new MathFunctions(_p5));
    });

    test('mag & magSq', () => {
	let v = MF.createVector(3.0,4.0);
	expect(v.mag()).toBeCloseTo(5.0);
	expect(v.magSq()).toBeCloseTo(25.0);
    });
    
    test('setMag', () => {
	let v = MF.createVector(3.0,4.0);
	v.setMag(15.0);
	expect(v.x).toBeCloseTo(9.0);
    });
    
    test('copy', () => {
	let v = MF.createVector(3.0,4.0);
	let v2 = v.copy();
	v2.x = 2.0;
	expect(v.x).not.toEqual(v2.x);
	expect(v.y).toEqual(v2.y);
    });

    test('normalize', () => {
	let v = MF.createVector(3.0,4.0);
	expect(v.normalize().y).toBeCloseTo(4.0/5.0);
    });
    
    test('add & sub', () => {
	let v = MF.createVector(3.0,4.0);
	let v2 = MF.createVector(-2.0,0.5);
	expect(v.add(v2).y).toBeCloseTo(4.5);
	expect(v2.sub(v).x).toBeCloseTo(-3.0);
    });

    test('setMag', () => {
	let v = MF.createVector(3.0,4.0);
	expect(v.setMag(15.0).x).toBeCloseTo(9.0);
    });
    
    test('div & mult', () => {
	let v = MF.createVector(3.0,4.0);
	expect(v.div(2.0).y).toBeCloseTo(4.0/2.0);
	expect(v.mult(3.0).x).toBeCloseTo(3.0*3.0/2.0);
    });
    
    test('limit', () => {
	let v = MF.createVector(3.0,4.0);
	expect(v.limit(2.0).x).toBeCloseTo(3.0*2.0/5.0);
    });
});
