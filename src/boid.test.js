import Boid from './boid';

//needed to start math fuinctions
import MathFunctions from './MathFunctions';
import p5 from "p5";
import 'jest-canvas-mock';
var _p5 = new p5();

describe('boid algorithms', () => {
    var MF;
    
    beforeAll(() => {
	return (MF = new MathFunctions(_p5));
    });

    test('constructor & initialize', () => {
	var flock = { width: 500, height:321 };
	var boid1 = new Boid(MF,flock,false);

	boid1.initialize(
	    {x:140,y:220},
	    {x:3.0,y:1.0},
	    "blue",
	    5.0
	);

	expect(boid1.pos.x).toBe(140);
	expect(boid1.pos.y).toBe(220);
	
	expect(boid1.vel.x).toBe(3.0);
	expect(boid1.vel.y).toBe(1.0);

	expect(boid1.color).toBe("blue");
	expect(boid1.size).toBe(5.0);
    });

    test('initialized fwdDir', () => {
	var flock = { width: 500, height:321 };
	var boid1 = new Boid(MF,flock,false);

	boid1.initialize(
	    {x:140,y:220},
	    {x:3.0,y:4.0},
	    "blue",
	    5.0
	);

	//fwdDir is defined as long as init velocity is not zero
	expect(boid1.fwdDir.x).toBeCloseTo(3.0/5.0);
	expect(boid1.fwdDir.y).toBeCloseTo(4.0/5.0);
    });

    test('initialized clipped & edges', () => {
	var flock = { width: 500, height:400 };

	//array of tests. pos created, pos clipped
	var testCoords = [
	    [{x:-1850,y:-880},{x:150,y:320}],
	    [{x:-440,y:60},{x:60,y:60}],
	    [{x:-640.4,y:720},{x:359.6,y:320}],
	    [{x:320,y:-20.4},{x:320,y:379.6}],
	    [{x:160.2,y:130.8},{x:160.2,y:130.8}],
	    [{x:450,y:450},{x:450,y:50}],
	    [{x:640,y:-220.87},{x:140,y:179.13}],
	    [{x:1850,y:220.87},{x:350,y:220.87}],
	    [{x:3630.4,y:1220.87},{x:130.4,y:20.87}]
	];

	for (var test of testCoords){	    
	    var boid1 = new Boid(MF,flock,false);
	    boid1.initialize(test[0],{x:3.0,y:4.0},"blue",5.0);
	    
	    expect(boid1.pos.x).toBeCloseTo(test[1].x);
	    expect(boid1.pos.y).toBeCloseTo(test[1].y);

	    var boid2 = new Boid(MF,flock,false);
	    boid2.pos.x = test[0].x;
	    boid2.pos.y = test[0].y;

	    boid2.edges(flock);
	    expect(boid2.pos.x).toBeCloseTo(test[1].x);
	    expect(boid2.pos.y).toBeCloseTo(test[1].y);
	}
    });

    test('closestRepresentative & distanceToSq',() => {
	var flock = { width: 500, height:400 };

	//each corner boid has to adapt his representative of each other.
	//center is placed so there is no need.

	var boids = [
	    new Boid(MF,flock,false).initialize({x:20,y:40},{x:0.0,y:1.0},"red",5.0), //NW
	    new Boid(MF,flock,false).initialize({x:480,y:30},{x:0.0,y:1.0},"red",5.0), //NE
	    new Boid(MF,flock,false).initialize({x:10,y:380},{x:0.0,y:1.0},"red",5.0), //SW
	    new Boid(MF,flock,false).initialize({x:490,y:390},{x:0.0,y:1.0},"red",5.0), //SE
	    new Boid(MF,flock,false).initialize({x:260,y:190},{x:0.0,y:1.0},"red",5.0), //center
	];


	
	var expectedMatrix = [
	    [{x: 20,y: 40,d:    0},    {x:-20,y: 30,d: 1700},   {x: 10,y:-20,d: 3700},   {x:-10,y:-10,d: 3400},   {x:260,y:190,d:80100}], //NW perspective
	    [{x:520,y: 40,d: 1700},    {x:480,y: 30,d:    0},   {x:510,y:-20,d: 3400},   {x:490,y:-10,d: 1700},   {x:260,y:190,d:74000}], //NE perspective
	    [{x: 20,y:440,d: 3700},    {x:-20,y:430,d: 3400},   {x: 10,y:380,d:    0},   {x:-10,y:390,d:  500},   {x:260,y:190,d:98600}], //SW perspective
	    [{x:520,y:440,d: 3400},    {x:480,y:430,d: 1700},   {x:510,y:380,d:  500},   {x:490,y:390,d:    0},   {x:260,y:190,d:92900}], //SE perspective    
	    [{x: 20,y: 40,d:80100},    {x:480,y: 30,d:74000},   {x: 10,y:380,d:98600},   {x:490,y:390,d:92900},   {x:260,y:190,d:    0}]]; //center perspective

	for(var i = 0; i < 5; ++i){
	    for(var j = 0; j < 5; ++j){
		var pos2 = boids[i].closestRepresentative(boids[j].pos,true);
		expect(pos2.x).toBeCloseTo(expectedMatrix[i][j].x);
		expect(pos2.y).toBeCloseTo(expectedMatrix[i][j].y);
		
		expect(boids[i].distanceToSq(boids[j].pos,true)).toBeCloseTo(expectedMatrix[i][j].d);
	    }
	}
    });
});
